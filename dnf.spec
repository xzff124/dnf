%global py3pluginpath %{python3_sitelib}/%{name}-plugins
%global relate_libdnf_version 0.66.0

Name:                 dnf
Version:              4.14.0
Release:              13
Summary:              A software package manager that manages packages on Linux distributions.
License:              GPLv2+ and GPLv2 and GPL
URL:                  https://github.com/rpm-software-management/dnf
Source0:              https://github.com/rpm-software-management/dnf/archive/%{version}/%{name}-%{version}.tar.gz

Patch9000:            unlock-pidfile-if-current-proccess-is-NOT-dnf.patch
Patch9001:            fix-pid-file-residue.patch
Patch9002:            add-rpm-transaction-debuginfo.patch
Patch9003:            adapt-test-another-process.patch
Patch9004:            fix-dnf-history-undo-error-when-history-sqlite-missing.patch
Patch9005:            huawei-lock-file-add-verify-parameter.patch

%if 0%{?check_target_before_unlink}
Patch9006:            get-lockfile-exists-before-unlick.patch
%endif

%ifarch sw_64
Patch6000:            dnf-4.10.0-sw.patch
%endif
%if 0%(test `uname -m` == "loongarch64" && echo 1)
Patch6001:            0001-Add-loongarch-architecture-support.patch
%endif
Patch6002:            backport-fix-plugins-unit-tests-unload-plugins-upon-their-deletion.patch
Patch6003:            backport-pass-whole-url-in-relativeUrl-to-packageTarget-for-rpm-url-download.patch
Patch6004:            backport-add-support-for-rollback-of-group-upgrade-rollback.patch
Patch6005:            backport-ignore-processing-variable-files-with-unsupported-encoding.patch
Patch6006:            backport-fix-AttributeError-when-IO-busy-and-press-ctrl-c.patch

BuildArch:            noarch
BuildRequires:        cmake gettext systemd bash-completion python3-sphinx
Requires:             python3-%{name} = %{version}-%{release} libreport-filesystem
Recommends:           (%{_bindir}/sqlite3 if bash-completion) (python3-dbus if NetworkManager)
Provides:             dnf-command(alias) dnf-command(autoremove) dnf-command(check-update) dnf-command(clean)
Provides:             dnf-command(distro-sync) dnf-command(downgrade) dnf-command(group)  dnf-command(history)
Provides:             dnf-command(info) dnf-command(install) dnf-command(list) dnf-command(makecache)
Provides:             dnf-command(mark) dnf-command(provides) dnf-command(reinstall) dnf-command(remove)
Provides:             dnf-command(repolist) dnf-command(repoquery) dnf-command(repository-packages)
Provides:             dnf-command(search) dnf-command(updateinfo) dnf-command(upgrade) dnf-command(upgrade-to)
Conflicts:            python2-dnf-plugins-core < 4.0.6 python3-dnf-plugins-core < 4.0.6

%description
DNF is a software package manager that installs, updates, and removespackages
on RPM-based Linux distributions. It automatically computes dependencies and
determines the actions required to install packages.DNF also makes it easier
to maintain groups of machines, eliminating the need to manually update each
one using rpm.

%package -n           yum
Requires:             %{name} = %{version}-%{release}
Summary:              Package manager

%description -n       yum
Utility that allows users to manage packages on their systems.\
It supports RPMs, modules and comps groups & environments.

%package -n           python3-%{name}
Summary:              Python 3 interface to DNF
%{?python_provide:%python_provide python3-%{name}}
BuildRequires:        python3-devel python3-hawkey >= 0.66.0 python3-libdnf >= 0.66.0
BuildRequires:        python3-libcomps >= 0.1.8 libmodulemd >= 2.9.3
BuildRequires:        python3-gpg python3-rpm >= 4.14.0
Requires:             python3-gpg %{name}-data = %{version}-%{release} libmodulemd >= 2.9.3
Requires:             python3-hawkey >= 0.66.0 python3-libdnf >= %{relate_libdnf_version}
Requires:             python3-libcomps >= 0.1.8 python3-rpm >= 4.14.0
Recommends:           python3-unbound
Obsoletes:            python2-%{name}

%description -n python3-%{name}
Python 3 interface to DNF.

%package data
Summary:        Common data and configuration files for DNF
Requires:       libreport-filesystem
Obsoletes:      %{name}-conf < %{version}-%{release} %{name} < %{version}-%{release}
Provides:       %{name}-conf = %{version}-%{release}
 
%description data
Common data and configuration files for DNF

%package automatic
Summary:        %{pkg_summary} - automated upgrades
BuildRequires:  systemd
Requires:       %{name} = %{version}-%{release}
%{?systemd_requires}
 
%description automatic
Systemd units that can periodically download package upgrades and apply them.

%package              help
Summary:              Documents for dnf and yum
Buildarch:            noarch
Requires:             man info
Provides:             yum-help = %{version}-%{release}
Obsoletes:            yum-help < %{version}-%{release}

%description          help
Man pages and other related documents for dnf and yum

%prep
%autosetup -p1
mkdir build-py3

#Failure use case
#It not have root permissions in obs
#tests/api/test_dnf_base.py test_do_transaction
#tests/api/test_dnf_logging.py test_dnf_logger
sed -i '168,174d' tests/api/test_dnf_base.py
sed -i '51,55d' tests/api/test_dnf_logging.py

%build
pushd build-py3
%cmake .. -DPYTHON_DESIRED:FILEPATH=%{__python3}
%make_build all doc-man
popd

%install
pushd build-py3
%make_install
popd

%find_lang %{name}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/vars
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/aliases.d
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/plugins/
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/modules.d
mkdir -p %{buildroot}%{_sysconfdir}/bash_completion.d/dnf
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/modules.defaults.d
mkdir -p %{buildroot}%{py3pluginpath}/__pycache__/
mkdir -p %{buildroot}%{_localstatedir}/log/
mkdir -p %{buildroot}%{_var}/cache/dnf/
touch %{buildroot}%{_localstatedir}/log/%{name}.log
ln -sr %{buildroot}%{_bindir}/dnf-3 %{buildroot}%{_bindir}/dnf
mv %{buildroot}%{_bindir}/dnf-automatic-3 %{buildroot}%{_bindir}/dnf-automatic
rm -vf %{buildroot}%{_bindir}/dnf-automatic-*
mv -f %{buildroot}%{_sysconfdir}/%{name}/%{name}-strict.conf %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf
ln -sr  %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf %{buildroot}%{_sysconfdir}/yum.conf
ln -sr  %{buildroot}%{_bindir}/dnf-3 %{buildroot}%{_bindir}/yum
mkdir -p %{buildroot}%{_sysconfdir}/yum
ln -sr  %{buildroot}%{_sysconfdir}/%{name}/plugins %{buildroot}%{_sysconfdir}/yum/pluginconf.d
ln -sr  %{buildroot}%{_sysconfdir}/%{name}/protected.d %{buildroot}%{_sysconfdir}/yum/protected.d
ln -sr  %{buildroot}%{_sysconfdir}/%{name}/vars %{buildroot}%{_sysconfdir}/yum/vars

%check
export TERM=linux
pushd build-py3
ctest -VV
popd

%post
%systemd_post dnf-makecache.timer

%preun
%systemd_preun dnf-makecache.timer

%postun
%systemd_postun_with_restart dnf-makecache.timer

%post automatic
%systemd_post dnf-automatic.timer
%systemd_post dnf-automatic-notifyonly.timer
%systemd_post dnf-automatic-download.timer
%systemd_post dnf-automatic-install.timer

%preun automatic
%systemd_preun dnf-automatic.timer
%systemd_preun dnf-automatic-notifyonly.timer
%systemd_preun dnf-automatic-download.timer
%systemd_preun dnf-automatic-install.timer

%postun automatic
%systemd_postun_with_restart dnf-automatic.timer
%systemd_postun_with_restart dnf-automatic-notifyonly.timer
%systemd_postun_with_restart dnf-automatic-download.timer
%systemd_postun_with_restart dnf-automatic-install.timer

%files
%{_bindir}/%{name}
%{_sysconfdir}/bash_completion.d/%{name}
%{_unitdir}/%{name}-makecache.timer
%{_unitdir}/%{name}-makecache.service
%{_var}/cache/%{name}/

%files -n yum
%{_bindir}/yum
%{_sysconfdir}/yum/vars
%{_sysconfdir}/yum.conf
%{_sysconfdir}/yum/pluginconf.d
%{_sysconfdir}/yum/protected.d
%config(noreplace) %{_sysconfdir}/%{name}/protected.d/yum.conf

%files -n python3-%{name}
%{_bindir}/%{name}-3
%exclude %{python3_sitelib}/%{name}/automatic
%{python3_sitelib}/%{name}/
%dir %{py3pluginpath}
%dir %{py3pluginpath}/__pycache__

%files data
%license COPYING PACKAGE-LICENSING
%doc AUTHORS README.rst
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/modules.d
%dir %{_sysconfdir}/%{name}/modules.defaults.d
%dir %{_sysconfdir}/%{name}/plugins
%dir %{_sysconfdir}/%{name}/protected.d
%dir %{_sysconfdir}/%{name}/vars
%dir %{_sysconfdir}/%{name}/aliases.d
%exclude %{_sysconfdir}/%{name}/aliases.d/zypper.conf
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%config(noreplace) %{_sysconfdir}/%{name}/protected.d/%{name}.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%ghost %attr(644,-,-) %{_localstatedir}/log/hawkey.log
%ghost %attr(644,-,-) %{_localstatedir}/log/%{name}.log
%ghost %attr(644,-,-) %{_localstatedir}/log/%{name}.librepo.log
%ghost %attr(644,-,-) %{_localstatedir}/log/%{name}.rpm.log
%ghost %attr(644,-,-) %{_localstatedir}/log/%{name}.plugin.log
%ghost %attr(755,-,-) %dir %{_sharedstatedir}/%{name}
%ghost %attr(644,-,-) %{_sharedstatedir}/%{name}/groups.json
%ghost %attr(755,-,-) %dir %{_sharedstatedir}/%{name}/yumdb
%ghost %attr(755,-,-) %dir %{_sharedstatedir}/%{name}/history
%{_tmpfilesdir}/%{name}.conf
%{_sysconfdir}/libreport/events.d/collect_dnf.conf

%files automatic
%{_bindir}/%{name}-automatic
%config(noreplace) %{_sysconfdir}/%{name}/automatic.conf
%{_unitdir}/%{name}-automatic.service
%{_unitdir}/%{name}-automatic.timer
%{_unitdir}/%{name}-automatic-notifyonly.service
%{_unitdir}/%{name}-automatic-notifyonly.timer
%{_unitdir}/%{name}-automatic-download.service
%{_unitdir}/%{name}-automatic-download.timer
%{_unitdir}/%{name}-automatic-install.service
%{_unitdir}/%{name}-automatic-install.timer
%{python3_sitelib}/%{name}/automatic/

%files help
%{_datadir}/locale/*
%{_datadir}/bash-completion/*
%{_mandir}/man8/yum.8*
%{_mandir}/man8/yum2dnf.8*
%{_mandir}/man8/%{name}.8*
%{_mandir}/man5/yum.conf.5.*
%{_mandir}/man8/yum-shell.8*
%{_mandir}/man1/yum-aliases.1*
%{_mandir}/man5/%{name}.conf.5*
%{_mandir}/man5/dnf-transaction-json.5.gz
%{_mandir}/man7/dnf.modularity.7*
%{_mandir}/man8/%{name}-automatic.8*

%changelog
* Mon Mar 13 2023 chenhaixing <chenhaixing@huawei.com> 4.14.0-13
- CVE:NA
- SUG:NA
- DESC:dnf:fix AttributeError when io busy and press ctrl c

* Tue Mar 7 2023 chenhaixing <chenhaixing@huawei.com> 4.14.0-12
- CVE:NA
- SUG:NA
- DESC:dnf:fix undo error when include rollback of group upgrade
           fix can't download package when url have characters encoded

* Wed Feb 22 2023 xiasenlin <xiasenlin1@huawei.com> - 4.14.0-11
- DESC:Add exception capture to get-lockfile-exists-before-unlick.patch

* Fri Feb 17 2023 chenhaixing <chenhaixing@huawei.com> 4.14.0-10
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:dnf:fix install error when vim file in /etc/dnf/vars/

* Tue Jan 10 2023 xiasenlin <xiasenlin1@huawei.com> - 4.14.0-9
- DESC:add get-lockfile-exists-before-unlick.patch

* Fri Jan 6 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 4.14.0-8
- DESC: fix build error for loongarch64

* Tue Dec 20 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-7
- DESC:fix test cases failed in tests/test_plugin.py

* Thu Dec 15 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-6
- DESC:remove tests

* Tue Dec 13 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-5
- DESC:fix dnf-data downgrade confilct with dnf

* Fri Dec 9 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-4
- DESC:add sw64 and loongarch support for dnf

* Fri Dec 9 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-3
- DESC:enable DT

* Wed Nov 16 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-2
- DESC:correct the incorrect Obsoletes

* Thu Nov 3 2022 chenhaixing <chenhaixing@huawei.com> - 4.14.0-1
- DESC:upgrade dnf version to 4.14.0

* Mon Mar 21 2022 yangcheng <yangcheng87@h-partners.com> - 4.10.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix dnf error when history sqlite missing

* Mon Jan 17 2022 liuyumeng <liuyumeng5@huawei.com> - 4.10.0-2
- round self-developed patch,adapt test_onother_process

* Wed Dec 29 2021 yangcheng <yangcheng87@huawei.com> - 4.10.0-1
- upgrade to 4.10.0

* Thu Jul 15 2021 gaihuiying <gaihuiying1@huawei.com> - 4.2.23-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove recommends rpm-plugins-systemd-inhibit
       backport community patches and fix CVE-2021-3445
       delete duplicate python3-libdnf dependency

* Tue Mar 30 2021 gaihuiying <gaihuiying1@huawei.com> - 4.2.23-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix errors when use "yum module remove --all per:common" command
       fix test test_mode_tty failed

* Fri Nov 20 2020 lunankun <lunankun@huawei.com> - 4.2.23-4
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove depend deltarpm

* Tue Sep 01 2020 zhangrui <zhangrui182@huawei.com> - 4.2.23-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:provide automatic files and provide yum help

* Tue Aug 04 2020 yuboyun <yuboyun@huawei.com> - 4.2.23-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix conflicts error for new version

* Tue Apr 28 2020 zhouyihang <zhouyihang3@huawei.com> - 4.2.23-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update dnf version to 4.2.23

* Wed Mar 18 2020 songnannan <songnannan2@huawei.com> - 4.2.15-8
- add obsoletes the python2-dnf

* Tue Mar 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.15-7
- modify the patch name

* Thu Feb 27 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.15-6
- remove extra brace

* Mon Feb 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.15-5
- Revert Fix messages for starting and failing scriptlets

* Fri Feb 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.15-4
- remove python2

* Fri Jan 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.15-3
- bug fix format problem

* Mon Jan 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.15-2
- Package Init
